CREATE DATABASE Hotelss_db;
USE Hotelss_db;


CREATE TABLE customers(customer_id INTEGER  AUTO_INCREMENT PRIMARY KEY,first_name VARCHAR (50) NOT NULL,last_name VARCHAR (50) NOT NULL,email VARCHAR (50) UNIQUE NOT NULL,password VARCHAR (50) NOT NULL,contact_no VARCHAR (50) NOT NULL,date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

 INSERT INTO customers(first_name,last_name,email,password,contact_no) VALUES("Sachin","Powar","sachin@gmail.com","Sachin@123","9956864879");




CREATE TABLE admin (admin_id INTEGER  AUTO_INCREMENT PRIMARY KEY,first_name VARCHAR (50) NOT NULL,last_name VARCHAR (50) NOT NULL,email VARCHAR (50) UNIQUE NOT NULL,password VARCHAR (50) NOT NULL,contact_no VARCHAR (50) NOT NULL,date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

INSERT INTO admin (first_name,last_name,email,password,contact_no) VALUES("Sachin","Powar","sachin@gmail.com","Sachin@123","8411920448");


 
CREATE TABLE room(room_id INTEGER  AUTO_INCREMENT PRIMARY KEY,room_type VARCHAR (50) NOT NULL,description VARCHAR (1000) NOT NULL,price INTEGER NOT NULL,image VARCHAR (30) NOT NULL,date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

INSERT INTO room(room_type,description,price,image) VALUES ("Ac_double_bed","Air-conditioned WI-FI Breakfast television telephone",2500,"Ac_doublebed.jpg");
INSERT INTO room(room_type,description,price,image) VALUES ("Ac_Single_Bed","Air-conditioned WI-FI Breakfast television telephone",1500,"Ac_SingleBed.jpg");
INSERT INTO room(room_type,description,price,image) VALUES ("Non_AcSingle_bed","Breakfast telephone",1000,"Non_AcSinglebed.jpg");
INSERT INTO room(room_type,description,price,image) VALUES ("NonAc_double_bed","WIFI Breakfast television telephone",1600,"NonAc_doublebed.jpg");
INSERT INTO room(room_type,description,price,image) VALUES ("Ac_Single","WIFI Breakfast television telephone",2000,"AcSingle_bed.jpg");
INSERT INTO room(room_type,description,price,image) VALUES ("Ac_double_bed","WIFI Breakfast television telephone",4000,"AcDoublebed.jpg");
INSERT INTO room(room_type,description,price,image) VALUES ("NonAc_Single_bed","WIFI Breakfast television telephone",900,"NonAc_singlebed.jpg");
INSERT INTO room(room_type,description,price,image) VALUES ("NonAc_double_bed","WIFI Breakfast television telephone",1800,"Non_AcDoublebed.jpg");


CREATE TABLE address(address_id INTEGER PRIMARY KEY AUTO_INCREMENT,customer_id INTEGER,FOREIGN KEY(customer_id)REFERENCES customers(customer_id),address VARCHAR (200),state VARCHAR(50) NOT NULL,city VARCHAR(50) NOT NULL,pincode INTEGER NOT NULL);

INSERT INTO address(customer_id,address,state,city,pincode) VALUES (1,"Jawahar Nagar","Maharastra","Pune",411109);



CREATE TABLE cart(cart_id INTEGER PRIMARY KEY AUTO_INCREMENT,book_id INTEGER,FOREIGN KEY(book_id)REFERENCES room(room_id),customer_id INTEGER,FOREIGN KEY(customer_id)REFERENCES customers(customer_id),quantity INTEGER, price FLOAT NOT NULL,total FLOAT NOT NULL,date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

INSERT INTO cart(book_id,customer_id,quantity,price,total) VALUES(1,1,2,500,1000);





CREATE TABLE booking(
    book_id INT PRIMARY KEY AUTO_INCREMENT,
    customer_id INT,
    room_id INT,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (room_id) REFERENCES room(room_id) ON DELETE CASCADE ON UPDATE CASCADE
);