package com.example.food_delivery.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.food_delivery.R;
import com.example.food_delivery.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class SigninActivity extends AppCompatActivity {
    EditText editEmail,editPassword;
    CheckBox checkRememberMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);
        checkRememberMe = findViewById(R.id.checkRememberMe);
    }
    public void signin(View view){
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        if(email.equals(""))
            Toast.makeText(this, "Email cannot be empty", Toast.LENGTH_SHORT).show();
        else if(password.equals(""))
            Toast.makeText(this, "Password cannot be empty", Toast.LENGTH_SHORT).show();
        else{
            JSONObject object = new JSONObject();
            try {
                object.put("email",email);
                object.put("password",password);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            verifyUser(object);
        }

    }
    private void verifyUser(JSONObject object) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constants.getURL("/signin"), object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {if((response.getString("status").equals("success")) && (!response.getJSONArray("data").isNull(0))){
                    JSONObject object = response.getJSONArray("data").getJSONObject(0);
                    Log.e("Customers",object.toString());

                    if(checkRememberMe.isChecked())
                        getSharedPreferences("signin",0).edit().putBoolean("signin_status",true).apply();

                    getSharedPreferences("signin",0).edit().putInt("customer_id",object.getInt("customer_id")).apply();
                    startActivity(new Intent(SigninActivity.this,MainActivity.class));
                    finish();
                }
                    else
                        Toast.makeText(SigninActivity.this, "Invalid Credentials.. :(", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    public void signup(View view){
        startActivity(new Intent(this,SignupActivity.class));
    }
}