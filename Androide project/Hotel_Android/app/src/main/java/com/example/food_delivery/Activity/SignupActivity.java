package com.example.food_delivery.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.SyncStateContract;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.food_delivery.R;
import com.example.food_delivery.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends AppCompatActivity {
EditText editfirst_name,editlast_name,editEmail,editPassword,editContact_no,editConfirmPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        editfirst_name=findViewById(R.id.editfirst_name);
        editlast_name=findViewById(R.id.editlast_name);
        editEmail=findViewById(R.id.editEmail);
        editPassword=findViewById(R.id.editPassword);
        editContact_no=findViewById(R.id.editContact_no);
        editConfirmPassword=findViewById(R.id.editConfirmPassword);

    }
    public void signup(View view){

            String first_name = editfirst_name.getText().toString();
            String last_name = editlast_name.getText().toString();
            String email = editEmail.getText().toString();
            String password = editPassword.getText().toString();
            String contact_no = editContact_no.getText().toString();
            String confirmpassword = editConfirmPassword.getText().toString();
            if(first_name.equals(""))
                Toast.makeText(this, "First Name cannot be empty", Toast.LENGTH_SHORT).show();
            else if(last_name.equals(""))
            Toast.makeText(this, "Last Name cannot be empty", Toast.LENGTH_SHORT).show();
            else if(email.equals(""))
                Toast.makeText(this, "Email cannot be empty", Toast.LENGTH_SHORT).show();
            else if(password.equals(""))
                Toast.makeText(this, "Password cannot be empty", Toast.LENGTH_SHORT).show();
            else if(contact_no.equals(""))
                Toast.makeText(this, "Contact_No cannot be empty", Toast.LENGTH_SHORT).show();
            else if(confirmpassword.equals(""))
                Toast.makeText(this, "Confirm password cannot be empty", Toast.LENGTH_SHORT).show();
            else {
                if(password.equals(confirmpassword))
                {
                    JSONObject object = new JSONObject();
                    try {
                        object.put("first_name",first_name);
                        object.put("last_name",last_name);
                        object.put("email",email);
                        object.put("password",password);
                        object.put("contact_no",contact_no);

                        signupCustomer(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                    Toast.makeText(this, "password does not match ..", Toast.LENGTH_SHORT).show();
            }

    }

    private void signupCustomer(JSONObject object) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,Constants.getURL("/signup"), object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("status").equals("success"))
                        finish();
                    else
                        Toast.makeText(SignupActivity.this, "Registration Failed.. Try with different Email", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }
    public void cancel(View view){
        finish();

    }
}