package com.example.food_delivery.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.food_delivery.R;
import com.example.food_delivery.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFragment extends Fragment {
Toolbar toolbar;
TextView textcustomer_id,textfirst_name,textlast_name,textemail,textcontact_no;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar=view.findViewById(R.id.toolbar);
        textcustomer_id=view.findViewById(R.id.textcustomer_id);
        textfirst_name=view.findViewById(R.id.textfirst_name);
        textlast_name=view.findViewById(R.id.textlast_name);
        textemail=view.findViewById(R.id.textemail);
        textcontact_no=view.findViewById(R.id.textcontact_no);

        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        getCustomerData(  getContext().getSharedPreferences("signin",0).getInt("customer_id",0));
    }
private void getCustomerData(int customer_id){
    Volley.newRequestQueue(getContext()).add(new JsonObjectRequest(
            Constants.getURL("/signin/" + customer_id), new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            try {
                if(response.getString("status").equals("success")){
                    JSONObject object=response.getJSONArray("data").getJSONObject(0);
                    textcustomer_id.setText("customer_id : "+object.getInt("customer_id"));
                    textfirst_name.setText("First_Name : "+object.getString("first_name"));
                    textlast_name.setText("Last_Name : "+object.getString("last_name"));
                    textemail.setText("Email : "+object.getString("email"));
                    textcontact_no.setText("Contact_No : "+object.getString("contact_no"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {

        }
    }
    ));

}
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        getContext().getSharedPreferences("signin",0).edit().putBoolean("signin_status",false).apply();
        getActivity().finish();
        return super.onOptionsItemSelected(item);

    }
}