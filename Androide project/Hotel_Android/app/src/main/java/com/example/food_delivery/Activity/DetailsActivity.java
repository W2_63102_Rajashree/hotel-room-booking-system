package com.example.food_delivery.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.food_delivery.R;
import com.example.food_delivery.entities.Room;
import com.example.food_delivery.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailsActivity extends AppCompatActivity {
    TextView textRoomtype, textdescription, textprice;
    ImageView imageView;
    int room_id;
    Button btnPlaceOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        btnPlaceOrder = findViewById(R.id.btnPlaceOrder);
        textRoomtype = findViewById(R.id.textRoomtype);
        textdescription = findViewById(R.id.textdescription);
        textprice = findViewById(R.id.textprice);
        imageView = findViewById(R.id.imgView);
        displayMenu();

    }

    private void displayMenu() {
        Room room = (Room) getIntent().getSerializableExtra("room");
        room_id = room.getRoom_id();

        textRoomtype.setText("Room_type : " + room.getRoom_type());
        textdescription.setText("Description : " + room.getDescription());
        textprice.setText("Price : " + room.getPrice());

        String imgUrl = Constants.getURL("/" + room.getImage());
        Glide.with(DetailsActivity.this).load(imgUrl).into(imageView);

        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderNow(room);

            }
        });

    }

    public void orderNow(Room room) {
        int customer_id = getSharedPreferences("signin", 0).getInt("customer_id", 0);
        JSONObject object = new JSONObject();
        try {
            object.put("customer_id", customer_id);
            object.put("room_id", room.getRoom_id());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Volley.newRequestQueue(this).add(new JsonObjectRequest(Request.Method.POST, Constants.getURL("/addorder"), object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("status").equals("success")) {
                        Toast.makeText(DetailsActivity.this, "Ordered", Toast.LENGTH_SHORT).show();
                        finish();

                    } else {
                        Toast.makeText(DetailsActivity.this, "Order Cancelled due to something Error...", Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }));
        Toast.makeText(this, "Ordered", Toast.LENGTH_SHORT).show();
    }
}