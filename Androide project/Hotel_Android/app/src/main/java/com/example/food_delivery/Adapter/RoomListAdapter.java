package com.example.food_delivery.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.food_delivery.Activity.DetailsActivity;
import com.example.food_delivery.R;
import com.example.food_delivery.entities.Room;
import com.example.food_delivery.utils.Constants;

import java.util.List;

public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.ViewHolder> {
    List<Room> roomList;

    public RoomListAdapter(List<Room> roomList) {
        this.roomList = roomList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Room room = roomList.get(position);
        holder.room_type.setText("Room_Type: " + room.getRoom_type());
       // holder.textdescription.setText("Description: " + room.getDescription());
        holder.textprice.setText("Price: " + room.getPrice());
        Glide.with(holder.imageView).load(Constants.getURL("/" + room.getImage())).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return roomList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView room_type, textdescription, textprice;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            room_type = itemView.findViewById(R.id.textRoomtype);
            textdescription = itemView.findViewById(R.id.textdescription);
            textprice = itemView.findViewById(R.id.textprice);
            imageView = itemView.findViewById(R.id.imageview);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(v.getContext(), DetailsActivity.class);
            intent.putExtra("room", roomList.get(getAdapterPosition()));
            v.getContext().startActivity(intent);
        }
    }

}
