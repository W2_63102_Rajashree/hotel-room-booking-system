package com.example.food_delivery.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.example.food_delivery.Adapter.OrderListAdapter;
import com.example.food_delivery.R;
import com.example.food_delivery.entities.Room;
import com.example.food_delivery.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OrdersFragment extends Fragment {

    RecyclerView recyclerView;
    List<Room> rooms;
    OrderListAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orders, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        getorder(getContext().getSharedPreferences("signin",0).getInt("customer_id",0));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rooms=new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        adapter=new OrderListAdapter(rooms);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

    }

    private void getorder(int customer_id){
        rooms.clear();
        Volley.newRequestQueue(getContext()).add(new JsonObjectRequest(Constants.getURL("/orders/"+customer_id), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ORDERS", "onResponse: response" + response);

                        try {
                            if (response.getString("status").equals("success")){
                                JSONArray jsonArray=response.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject object=jsonArray.getJSONObject(i);
                                    Room room=new Room();
                                    room.setRoom_id(object.getInt("room_id"));
                                    room.setRoom_type(object.getString("room_type"));
                                    room.setDescription(object.getString("description"));
                                    room.setPrice(object.getInt("price"));
                                    room.setImage(object.getString("image"));
                                    rooms.add(room);

                                    Log.d("ORDERS", "onResponse: menus" +room);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })
        );
    }

}