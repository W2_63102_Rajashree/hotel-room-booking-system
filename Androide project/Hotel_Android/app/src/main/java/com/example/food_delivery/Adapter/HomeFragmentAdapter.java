package com.example.food_delivery.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.food_delivery.Fragments.RoomFragment;
import com.example.food_delivery.Fragments.OrdersFragment;
import com.example.food_delivery.Fragments.ProfileFragment;

public class HomeFragmentAdapter extends FragmentStateAdapter {
    public HomeFragmentAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment=new RoomFragment();
        switch (position){
            case 1:
                fragment=new OrdersFragment();
                break;
//            case 2:
//                fragment=new CartFragment();
//                break;
            case 2:
                fragment =new ProfileFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
