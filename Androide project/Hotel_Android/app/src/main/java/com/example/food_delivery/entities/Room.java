package com.example.food_delivery.entities;

import java.io.Serializable;

public class Room implements Serializable {
    private int room_id;
    private String room_type;
    private String description;
    private int price;
    private String image;

    public Room(String room_type, String description, int price, String image) {
        this.room_type = room_type;
        this.description = description;
        this.price = price;
        this.image = image;
    }

    public Room(int room_id, String room_type, String description, int price, String image) {
        this.room_id = room_id;
        this.room_type = room_type;
        this.description = description;
        this.price = price;
        this.image = image;
    }

    public Room() {

    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Room{" +
                "room_id=" + room_id +
                ", room_type='" + room_type + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", image='" + image + '\'' +
                '}';
    }
}
