package com.example.food_delivery.Adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.food_delivery.R;
import com.example.food_delivery.entities.Room;
import com.example.food_delivery.utils.Constants;

import java.util.List;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder> {
List<Room> orderList;

    public OrderListAdapter(List<Room> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list, parent, false);
        return new OrderListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Room room =orderList.get(position);
        holder.textRoomtype.setText("Room_Type: " + room.getRoom_type());
        holder.textdescription.setText("Description: " + room.getDescription());
        holder.textprice.setText("Price: " + room.getPrice());
        Glide.with(holder.imageView).load(Constants.getURL("/" + room.getImage())).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textRoomtype, textdescription, textprice;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textRoomtype = itemView.findViewById(R.id.textRoomtype);
            textdescription = itemView.findViewById(R.id.textdescription);
            textprice = itemView.findViewById(R.id.textprice);
            imageView = itemView.findViewById(R.id.imageview);


        }
    }
}
