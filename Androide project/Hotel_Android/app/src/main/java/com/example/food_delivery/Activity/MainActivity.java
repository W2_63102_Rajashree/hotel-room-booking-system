package com.example.food_delivery.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentContainerView;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import com.example.food_delivery.Adapter.HomeFragmentAdapter;
import com.example.food_delivery.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {
    FragmentContainerView fragmentContainer;
    ViewPager2 viewPager2;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentContainer = findViewById(R.id.fragmentContainer);
        viewPager2 = findViewById(R.id.viewPager2);
        tabLayout = findViewById(R.id.tabLayout);

        HomeFragmentAdapter adapter = new HomeFragmentAdapter(this);
        viewPager2.setAdapter(adapter);
        new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                switch (position) {
                    case 0:
                        tab.setIcon(R.drawable.room);
                        break;
                    case 1:
                        tab.setIcon(R.drawable.cart_icon);
                        break;
//                    case 2:
//                        tab.setIcon(R.drawable.order_icon);
//                        break;
                    case 2:
                        tab.setIcon(R.drawable.profile_icon);
                        break;
                }

            }
        }).attach();


    }
}