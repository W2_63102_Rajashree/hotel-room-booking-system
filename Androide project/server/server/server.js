const express=require('express')
const cors=require('cors')
//const utils=require('./utils')



//const routerMenu=require('./routes/menu/Menu')
const routerAddress = require('./routes/customers/Address')
const routerProfile = require('./routes/customers/Profile')
const routersignin = require('./routes/customers/signin')
const routersignup = require('./routes/customers/signup')
const routerAdminorder=require('./routes/Admin/Adminorder')
const routerAccept=require('./routes/Admin/Accept')
const routerallorders=require('./routes/Admin/allorders')
const routerasignin=require('./routes/Admin/asignin')
const routerorderdetails=require('./routes/Admin/orderdetails')
const routerAddroom=require('./routes/room/Addroom')
const routerAllroom=require('./routes/room/Allroom')
const routerEditroom=require('./routes/room/Editroom')
const routerSingleroom=require('./routes/room/Singleroom')
const routerAddbook=require('./routes/booking/Addbook')
const routerBook=require('./routes/booking/Book')
const routerViewbooking=require('./routes/booking/Viewbooking')
const routerAddtocart=require('./routes/cart/Addtocart')
const routeremptycart=require('./routes/cart/emptycart')
const routerSelectfromcart=require('./routes/cart/Selectfromcart')
const routerUpdatecart=require('./routes/cart/Updatecart')
const routerDeletefromcart=require('./routes/cart/Deletefromcart')
const routerDeleteroom=require('./routes/room/Deleteroom')




const app=express()
app.use(express.json())
app.use(cors('*'))
app.use(express.static('images'))




//app.use('/Room',routerRoom)
app.use(routerAddress)
app.use(routerProfile)
app.use(routersignin)
app.use(routersignup)
app.use(routerAdminorder)
app.use(routerAccept)
app.use(routerallorders)
app.use(routerasignin)
app.use(routerorderdetails)
app.use(routerAddroom)
app.use(routerAllroom)
app.use(routerDeleteroom)
app.use(routerSingleroom)
app.use(routerAddbook)
app.use(routerBook)
app.use(routerViewbooking)
app.use(routerAddtocart)
app.use(routeremptycart)
app.use(routerSelectfromcart)
app.use(routerUpdatecart)
app.use(routerDeletefromcart)
app.use(routerDeleteroom)







app.listen(4000,'0.0.0.0',()=>{
    console.log("server started at port number 4000");
})