const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

router.post('/signup', (request, response) => {
    const { first_name, last_name, email, password,contact_no } = request.body
    const statement = `INSERT INTO customers(first_name, last_name, email, password,contact_no ) VALUES(?,?,?,?,?)`
    db.pool.query(statement, [first_name, last_name, email, password,contact_no ], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})



module.exports = router
