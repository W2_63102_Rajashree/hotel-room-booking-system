const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()


router.post('/signin', (request, response) => {
    const { email, password } = request.body
    const statement = `SELECT * FROM customers WHERE email=? and password=?`
    db.pool.query(statement, [email, password], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.get('/signin/:id', (request, response) => {
    const id = request.params.id
    const statement = `SELECT * FROM customers WHERE customer_id=?`
    db.pool.query(statement, [id], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})
module.exports = router