const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

router.post('/customer_id/:id', (request, response) => {
    const { id } = request.params
    const {address, state,city, pincode } = request.body
    const statement = `INSERT INTO address(customer_id,address, state,city, pincode  ) VALUES(?,?,?,?,?)`
    

    db.pool.query(statement, [id,address,state,city,pincode], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})



module.exports = router
