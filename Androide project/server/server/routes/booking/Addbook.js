const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')


router.post('/addbook', (request, response) => {

    const { customer_id, room_id }= request.body
    
    const statement = 
    `insert into booking (customer_Id,room_id)
      values(?,?)
    `
    db.pool.query(statement,[customer_id,room_id] ,(error, request) => {
      
    response.send(utils.createResult(error, request))


    })
})


module.exports = router