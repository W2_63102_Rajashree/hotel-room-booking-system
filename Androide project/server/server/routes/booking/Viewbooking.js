const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')

router.get('/viewbook', (request, response) => {

    const statement = 
    `select c.product_Id,c.quantity,c.price,o.book_id 
    from cart c 
    inner join booking o
    on c.customer_Id=o.customer_Id
    where o.status="order placed";
    `
    db.pool.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))


    })
})
module.exports = router 