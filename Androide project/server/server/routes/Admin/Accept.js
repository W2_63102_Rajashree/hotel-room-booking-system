const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()


router.put('/Accept/:id',(request,response)=>{

    const { id } = request.params
    
 
    
    const statement=
        `update orders set status ="Order Accepted" where customer_id = ${id};
        `

   db.pool.query(statement, (error,result) => {
      
        response.send(utils.createResult(error, result))
    })
})

router.put('/Prepare/:id',(request,response)=>{

    const { id } = request.params
    
    
   
    const statement=
        `update orders set status ="Preparing" where customer_id = ${id};
         `

    db.pool.query(statement, (error,result) => {
     
        response.send(utils.createResult(error, result))
    })
})

router.put('/Out/:id',(request,response)=>{

    const { id } = request.params
    
    

    const statement=
        `update orders set status ="Out for delivery" where customer_id = ${id};
         `

    db.pool.query(statement, (error,result) => {
       
        response.send(utils.createResult(error, result))
    })
})

router.put('/Delivered/:id',(request,response)=>{

    const { id } = request.params
    
    
  
    const statement=
        `update orders set status ="Delivered" where customer_id = ${id};
         `

    db.pool.query(statement, (error,result) => {
        
        response.send(utils.createResult(error, result))
    })
})


module.exports = router







