const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

router.get('/placedorder', (request, response) => {
    
    //const {customer_Id }= request.params
    
   
    const statement = 
    `select o.order_id,o.total,m.food_title,m.price,c.quantity
    from orders o
    inner join cart c
    on o.customer_Id = c.customer_Id
    inner join menu m
    on m.menu_id = c.product_Id
    where status = "order placed";
    `
    
    
    db.pool.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))

   

    })
})
module.exports = router 




