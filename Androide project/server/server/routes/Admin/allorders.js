const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()


router.get('/allorders',(request, response) => {
    
    
    const statement = `
   select order_id,customer_id,total,status
   from orders
    `

    db.pool.query(statement, (error, request) => {
    
        response.send(utils.createResult(error, request))

        connection.end()

      })
})

router.get('/delivered',(request, response) => {


  const statement = `
  select
  order_id,status
  from
  orders
  where
  status  LIKE '%Delivered%'
  `
  
  

  db.pool.query(statement, (error, result) => {
  
 
      response.send(utils.createResult(error, result))
    })
})

router.get('/Cancelled',(request, response) => {


    const statement = `
    select
    order_id,status
    from
    orders
    where
    status  LIKE '%Cancelled%'
    `
    
    
  
    db.pool.query(statement, (error, result) => {
    
   
        response.send(utils.createResult(error, result))
      })
  })
module.exports = router