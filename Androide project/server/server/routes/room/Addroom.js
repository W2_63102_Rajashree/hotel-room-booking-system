const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()


router.post('/Addmenu', (request, response) => {
      const {title, description, price } = request.body
      const query = `insert into menu (food_title, description, price) values (?,?,?)`
      
      db.pool.query(query, [title,description,price],(error,result) => {
        response.send(utils.createResult(error, result))
        })
      
      })
      
      module.exports = router