const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

router.get('/:customerId', (request, response) => {

    const {customerId }= request.params

    const statement = 
    `select c.cart_id as cartid, c.quantity,c.price,c.total,
             m.food_title, m.description, m.price
     from cart c
     inner join menu m on
     c.product_Id = m.menu_id
     where c.customer_Id = ${customerId}
    `
    
    
    db.pool.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))


    })
})
module.exports = router 