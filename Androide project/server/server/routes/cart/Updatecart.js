const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

router.put('/:id/:quantity', (request, response) => {
    const { id, quantity }= request.params;


    const statement = 
    `update cart set quantity = ${quantity}, total = quantity*price
     where cart_id=${id};
    `
    
    
    db.pool.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))


    })
})
module.exports = router 