const express = require('express')
const db = require('../../db')
const utils = require('../../utils')

const router = express.Router()

router.delete('/deletecart/:id', (request, response) => {
    const { id }= request.params
    
    const statement = 
    `delete from cart
    WHERE cart_id = ${id}
    `
    
    
    db.pool.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))

    })
})
module.exports = router 