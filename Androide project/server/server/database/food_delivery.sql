CREATE DATABASE food_delivery_db;
USE food_delivery_db;


CREATE TABLE customers(customer_id INTEGER  AUTO_INCREMENT PRIMARY KEY,first_name VARCHAR (50) NOT NULL,last_name VARCHAR (50) NOT NULL,email VARCHAR (50) UNIQUE NOT NULL,password VARCHAR (50) NOT NULL,contact_no VARCHAR (50) NOT NULL,date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

 



CREATE TABLE admin (admin_id INTEGER  AUTO_INCREMENT PRIMARY KEY,first_name VARCHAR (50) NOT NULL,last_name VARCHAR (50) NOT NULL,email VARCHAR (50) UNIQUE NOT NULL,password VARCHAR (50) NOT NULL,contact_no VARCHAR (50) NOT NULL,date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

INSERT INTO admin (first_name,last_name,email,password,contact_no) VALUES("Yuvraj","Baravkar","yuvrajbaravkar@gmail.com","Yuvraj@123","8411920448");


 
CREATE TABLE menu(menu_id INTEGER  AUTO_INCREMENT PRIMARY KEY,food_title VARCHAR (50) NOT NULL,description VARCHAR (1000) NOT NULL,price INTEGER NOT NULL,image VARCHAR (30) NOT NULL,date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

INSERT INTO menu(food_title,description,price,image) VALUES ("veg thali","Some of the dishes served in the thali are Aamat",800,"veg_thali.jpg");





CREATE TABLE address(address_id INTEGER PRIMARY KEY AUTO_INCREMENT,customer_id INTEGER,FOREIGN KEY(customer_id)REFERENCES customers(customer_id),address VARCHAR (200),state VARCHAR(50) NOT NULL,city VARCHAR(50) NOT NULL,pincode INTEGER NOT NULL);

INSERT INTO address(customer_id,address,state,city,pincode) VALUES (1,"Jawahar Nagar","Maharastra","Pune",411109);



CREATE TABLE cart(cart_id INTEGER PRIMARY KEY AUTO_INCREMENT,product_id INTEGER,FOREIGN KEY(product_id)REFERENCES menu(menu_id),customer_id INTEGER,FOREIGN KEY(customer_id)REFERENCES customers(customer_id),quantity INTEGER, price FLOAT NOT NULL,total FLOAT NOT NULL,date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

INSERT INTO cart(product_id,customer_id,quantity,price,total) VALUES(1,1,2,500,1000);



/*
CREATE TABLE orders(order_id INTEGER PRIMARY KEY AUTO_INCREMENT,customer_id INTEGER,FOREIGN KEY(customer_id)REFERENCES customers(customer_id),total FLOAT NOT NULL,status VARCHAR (50),date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
INSERT INTO orders(customer_id,total,status) VALUES (1,1000,"delivered");
*/

CREATE TABLE orders(
    order_id INT PRIMARY KEY AUTO_INCREMENT,
    customer_id INT,
    menu_id INT,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (menu_id) REFERENCES menu(menu_id) ON DELETE CASCADE ON UPDATE CASCADE
);