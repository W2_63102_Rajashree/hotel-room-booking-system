create database Silver_hotel;
use Silver_hotel;

create table user (
  id integer primary key auto_increment,
  firstName varchar(600),
  lastName varchar(600),
  mobile bigint,
  email varchar(600) UNIQUE,
  password varchar(100),
  createdTimeStamp timestamp default CURRENT_TIMESTAMP
);

create table room (
  id integer primary key auto_increment,
  title varchar(600),
  description varchar(600),
  price float,
  thumbnail varchar(1000),
  createdTimeStamp timestamp default CURRENT_TIMESTAMP
);

create table booking (
  id integer primary key auto_increment,
  userId integer,
  foreign key(userid) references user(id),
  datePlaced TIMESTAMP,
  total float,
  status varchar(100),
  createdTimeStamp timestamp default CURRENT_TIMESTAMP
);

-- create table orderDetails (
--   id integer primary key auto_increment,
--   orderId integer,
--   foreign key(orderId) references orders(id),
--   menuId integer,
--   -- foreign key(menuId) references room(id),
--   price float,
--   quantity integer,
--   createdTimeStamp timestamp default CURRENT_TIMESTAMP
-- );

create table bookDetails (
  id integer primary key auto_increment,
  bookingId integer,
  roomId integer,
  price float,
  quantity integer,
  createdTimeStamp timestamp default CURRENT_TIMESTAMP
);

create table cart (
  id integer primary key auto_increment,
  roomsId integer,
  foreign key(roomsId) references room(id),
  userId integer,
  foreign key(userId) references user(id),
  quantity integer,
  price integer,
  total integer,
  createdTimeStamp timestamp default CURRENT_TIMESTAMP
);

create table admin(
    adminid integer primary key auto_increment,
    firstName varchar(50),
    lastName varchar(50),
    email varchar(50),
    password varchar(50)
);

create table book(
  id integer primary key auto_increment,
  userId integer,
  foreign key(userId) references user(id),
  check_in datetime,
  check_out datetime,
  address varchar(200)
);

