const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const config = require('../../config')


router.post('/:id/:userId', (request, response) => {

    const connection = db.openConnection()
    const { id, userId }= request.params
    
    
    const statement = 
   
    `insert into cart (userId, roomsId, quantity, price, total)
     values(${userId},${id},1,(select price from room where id=${id}),
     quantity*price)
    `
    connection.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))

    connection.end()

    })
})
module.exports = router 