const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const config = require('../../config')

router.delete('/:userId', (request, response) => {
    const connection = db.openConnection()
    const { userId }= request.params
    
    const statement = 
    `delete from cart
    WHERE userId = ${userId}
    `
    
    
    connection.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))

    connection.end()

    })
})
module.exports = router 