const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const config = require('../../config')

router.put('/:id/:quantity', (request, response) => {
    const connection = db.openConnection()
    const { id, quantity }= request.params;

    
    // console.log(request.userId)
    const statement = 
    `update cart set quantity = ${quantity}, total = quantity*price
     where id=${id};
    `
    
    
    connection.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))

    connection.end()

    })
})
module.exports = router 