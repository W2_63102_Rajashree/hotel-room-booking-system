const { response } = require('express')
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const multer = require('multer')
const upload = multer({dest: 'images/'})

const router = express.Router()

//view all room
router.get('/Room',(request, response) => {
    const connection = db.openConnection()

    const statement = `
      select *
       from room
    `

    connection.query(statement, (error, request) => {
    
        response.send(utils.createResult(error, request))

        connection.end()

      })
})
module.exports = router