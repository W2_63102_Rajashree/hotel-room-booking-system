const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const jwt = require('jsonwebtoken')
const config = require('../../config')
const { request, response } = require('express')
const { resume } = require('../../db')

router.put('/Accept/:id',(request,response)=>{

    const { id } = request.params
    
    console.log("id"+id)
    const connection = db.openConnection()
    const statement=
        `update booking set status ="Booking Accepted" where id = ${id};
        `

    connection.query(statement, (error,result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.put('/Prepare/:id',(request,response)=>{

    const { id } = request.params
    
    
    const connection = db.openConnection()
    const statement=
        `update booking set status ="Preparing" where id = ${id};
         `

    connection.query(statement, (error,result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.put('/Out/:id',(request,response)=>{

    const { id } = request.params
    
    
    const connection = db.openConnection()
    const statement=
        `update booking set status ="Out for booking" where id = ${id};
         `

    connection.query(statement, (error,result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.put('/Booked/:id',(request,response)=>{

    const { id } = request.params
    
    
    const connection = db.openConnection()
    const statement=
        `update booking set status ="Booked" where id = ${id};
         `

    connection.query(statement, (error,result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

router.delete('/Delete/:id',(request,response)=>{
    
    const { id } = request.params
    const connection = db.openConnection()
    const statement=
        `delete od from bookDetails  od
        inner join booking o
        on o.id = od.bookingId 
        where o.id = ${id};
         `

    connection.query(statement, (error,result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})

module.exports = router