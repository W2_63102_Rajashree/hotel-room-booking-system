const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const config = require('../../config')


// router.post('/:orderId', (request, response) => {

    
//     const { orderId }= request.params
//     const { uorders }= request.body
    
//     for (const od of uorders)
//     {
//       const connection = db.openConnection()

//     const statement = 
//     `insert into orderdetails
//      (orderId, menuId, price, quantity)
//       values
//       (${orderId}, 1, ${od.price}, ${od.quantity})
//     `
    
//     connection.query(statement, (error, request) => {
      
//     response.send(utils.createResult(error, request))
    
//     connection.end()
    
//     })
//   }
// })

// router.post('/:userId', (request, response) => {
//   ;(async () => {
//     console.log(request.body)
//      const {userId} = request.params
//     // const { total, products } = request.body

//     const [order] = await db.execute(`
//     select c.productId,c.quantity,c.price,o.id
//     from cart c 
//     inner join orders o
//     on c.userId=o.userId
//     where o.status="order placed";
//     `)

//     // get the newly inserted records' id
//     // insert order details
   
//     for (const menu of order) {
//       await db.execute(`
//         insert into orderDetails 
//           (menuId, orderId, quantity, price)
//         values
//           (${menu.productId}, (select id from orders where userId = ${userId}), ${menu.quantity}, ${menu.price})
//       `)
//     }

//     // remove the items from cart
//     await db.execute(`delete from cart where userId = ${userId}`)

   
//   })
//   ()
// })

router.post('/:userId', (request, response) => {
  // async function is needed to use await inside it
  const bookDetails  = async () => {
  const {userId} = request.params
  const connection = await db.openConnection2()

  const statement = 
  `
  select roomsId,quantity,price
    from cart
    where 
    userId = ${userId}
  `

  const [data] = await connection.query(statement)

    for(let room of data){
        const [bookdetail] = await connection.query(
          `insert into bookDetails  
          (roomId,bookingId,quantity,price)
        values
          (${room.roomsId}, (select id from booking where userId = ${userId} AND status="Booking Confirm " order by id desc limit 1), ${room.quantity}, ${room.price})`
        )

        data['bookdetail'] = bookdetail

        const [deletecart] = await connection.query(
          `delete from cart where userId = ${userId}`
        )

        data['deletecart'] = deletecart
    }

    connection.end()
    response.send(utils.createResult(null, data))
  }
  bookDetails()
})
module.exports = router 