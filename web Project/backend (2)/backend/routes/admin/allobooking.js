const { response } = require('express')
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const multer = require('multer')
const upload = multer({dest: 'images/'})

const router = express.Router()

//view all room
router.get('/:bookingId',(request, response) => {
    const connection = db.openConnection()
    const { bookingId } = request.params
    const statement = `
    select od.bookingId,m.title,
    od.price,od.quantity,o.status
    from booking o
    inner join bookDetails  od
    on o.id = od.bookingId
    inner join room m
    on m.id = od.menuId 
    where 
    o.id = ${bookingId}
    group by bookingId,m.title;
    `
    // OR  o.status="Order Accepted" OR  o.status="Preparing" OR o.status="Out for Delivery";
    

    connection.query(statement, (error, request) => {
    
        response.send(utils.createResult(error, request))

        connection.end()

      })
})

router.get('/',(request, response) => {
  const connection = db.openConnection()

  const statement = `
  select
  id,status
  from
  booking
  where
  status NOT LIKE '%Booked%'
  `
  // OR  o.status="Order Accepted" OR  o.status="Preparing" OR o.status="Out for Delivery";
  

  connection.query(statement, (error, result) => {
  
    connection.end()
      response.send(utils.createResult(error, result))
    })
})
module.exports = router