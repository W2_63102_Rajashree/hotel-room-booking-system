const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const config = require('../../config')

router.get('/', (request, response) => {
    const connection = db.openConnection()
    const {userId }= request.params
    
    // console.log(request.userId)
    const statement = 
    `select o.id,o.total,m.title,m.price,c.quantity
    from booking o
    inner join cart c
    on o.userId = c.userId
    inner join room m
    on m.id = c.roomsId 
    where status = "Book room";
    `
    
    
    connection.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))

    connection.end()

    })
})
module.exports = router 