const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const cryptoJS = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')
const { request, response } = require('express')
const { resume } = require('../../db')

router.put('/edit/:id',(request,response)=>{

    const { id } = request.params
    const {mobile,email,password} = request.body
            // encrpt the password
  const encryptedPassword = String(cryptoJS.MD5(password))
    
    const connection = db.openConnection()
    const statement=
        `update user set
         mobile='${mobile}',
         email='${email}',
         password='${encryptedPassword}'
         where id= '${id}'
         `

    connection.query(statement, (error,result) => {
        connection.end()
        response.send(utils.createResult(error, result))
    })
})
module.exports = router