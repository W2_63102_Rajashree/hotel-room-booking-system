const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const config = require('../../config')

router.get('/:userId', (request, response) => {
    const connection = db.openConnection()
    const {userId }= request.params
    
    // console.log(request.userId)
    const statement = 
    `select c.id as cartid, c.quantity, m.id, m.title, m.description, m.price, m.thumbnail
     from cart c
     inner join room m on
     c.roomsId = m.id
     where c.userId = ${userId}
    `
    
    
    connection.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))

    connection.end()

    })
})
module.exports = router 