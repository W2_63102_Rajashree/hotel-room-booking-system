const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const cryptoJS = require('crypto-js')
//const jwt = require('jsonwebtoken')
//const config = require('../../config')


router.get('/test', (request, response) => {
  response.send({ "result": "hi from my app" });
})


router.post('/signin', (request, response) => {
    const { email, password } = request.body
    const connection = db.openConnection()
       // encrpt the password
       const encryptedPassword = String(cryptoJS.MD5(password))
    const statement =
    `select 
     id, firstName, lastName, mobile, 
     email, password from user 
     where email = '${email}' and 
     password = '${encryptedPassword}'
    `
       
    connection.query(statement, (error, users) => {
        connection.end()
    
        if (error) {
          response.send(utils.createResult(error))
        } else if (users.length == 0) {
          // there is no user matching the criterianodemon ser
          response.send(utils.createResult('user not found'))
        } else {
          const user = users[0]
          response.send(utils.createResult(null, user))
        }
    })
})
module.exports = router