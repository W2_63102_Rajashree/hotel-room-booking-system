const express = require('express')
const router = express.Router()
const db = require('../../db')
const cryptoJS = require('crypto-js')
const utils = require('../../utils')
const config = require('../../config')

router.post('/signup', (request, response) => {

  const { firstName, lastName, mobile, email, password } = request.body
    
  const connection = db.openConnection()
        // encrpt the password
  const encryptedPassword = String(cryptoJS.MD5(password))

  const statement = 
    `insert into user 
    (firstName, lastName, mobile, email, password) 
    values ('${firstName}', '${lastName}',
    '${mobile}','${email}','${encryptedPassword}')
    `
     
  connection.query(statement, (error,result) => {
  connection.end()
  response.send(utils.createResult(error, result))
  })

})

module.exports = router