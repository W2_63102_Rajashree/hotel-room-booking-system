const { response } = require('express')
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const multer = require('multer')
const upload = multer({dest: 'images/'})

const router = express.Router()

//view all booking
router.get('/:userId',(request, response) => {
    const connection = db.openConnection()

    const {userId} = request.params;

    const statement = `
      select id, total, status
       from booking where userId = ${userId}
    `

    connection.query(statement, (error, request) => {
    
        response.send(utils.createResult(error, request))

        connection.end()

      })
})
module.exports = router