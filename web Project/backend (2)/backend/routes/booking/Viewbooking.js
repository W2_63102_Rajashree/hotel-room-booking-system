const express = require('express')
const router = express.Router()
const db = require('../../db')
const utils = require('../../utils')
const config = require('../../config')

router.get('/', (request, response) => {
    const connection = db.openConnection()
   
    
    // console.log(request.userId)
    const statement = 
    `select c.roomsId,c.quantity,c.price,o.id
    from cart c 
    inner join booking o
    on c.userId=o.userId
    where o.status="Booking confirm";
    `
    connection.query(statement, (error, request) => {
    
    response.send(utils.createResult(error, request))

    connection.end()

    })
})
module.exports = router 