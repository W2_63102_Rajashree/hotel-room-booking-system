import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'


function UHome() {

  

    return (
        <Nav>
            <Title><span>SILVER HOTEL</span></Title>
            <NavMenu>
                <a>
                    <span><Link to="/User/my-booking" style={{ color: '#FFF' ,textDecoration: 'none' }}>My Booking</Link></span>
                </a>
                <a>
                    <span><Link to="/User/profile" style={{ color: '#FFF' ,textDecoration: 'none' }}>Profile</Link></span>
                </a>
                <a>
                    <span><Link to="/User/cart" style={{ color: '#FFF' ,textDecoration: 'none' }}>Cart</Link></span>
                </a>
                <a>
                    <span><Link to="/" style={{ color: '#FFF' ,textDecoration: 'none' }}>Logout</Link></span>
                </a>          
              </NavMenu>
        </Nav>
    )
}
export default UHome

const Title = styled.span`
    color: white;
    display: flex;
    flex:1;
    align-items: center;
    font-size: 30px;

`

const Nav = styled.nav`
    height: 50px;
    background:  #66c4f2;
    display: flex;
    justify-content: right;
    align-items:center;
    padding: 0 36px;
    overflow-x: hidden;

`


const NavMenu = styled.div`
    display: flex;

    margin-left: 25px;
    align-items: center;
    

    a{
        display: flex;
        align-items: center;
        padding: 0 12px;
        cursor: pointer;


        span {
            font-size: 15px;
            color: white;
            letter-spacing: 1.42px;
            position: relative;

            &:after {
                content: "";
                height: 2px;
                background: red;
                position: absolute;
                left:0;
                right:0;
                bottom:-6px;
                opacity:0;
                transition: all 250ms cubic-bezier(0.25,0.46,0.45,0.94) 0s;
                transform: scaleX(0);
            }
        }

        &:hover {
            span:after {
                transform: scaleX(1);
                opacity: 1;
            }
        }
    }


`

// const Dropdown = styled.div`
// ul {
//     list-style-type: none;
//     margin: 0;
//     padding: 0;
//     overflow: hidden;
//     background-color: #333;
//   }
  
//   li {
//     float: left;
//   }
  
//   li a, dropbtn {
//     display: inline-block;
//     color: white;
//     text-align: center;
//     padding: 14px 16px;
//     text-decoration: none;
//   }
  
//   li a:hover, dropdown:hover dropbtn {
//     background-color: red;
//   }
  
//   li dropdown {
//     display: inline-block;
//   }
  
//   dropdown-content {
//     display: none;
//     position: absolute;
//     background-color: #f9f9f9;
//     min-width: 160px;
//     box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
//     z-index: 1;
//   }
  
//   dropdown-content a {
//     color: black;
//     padding: 12px 16px;
//     text-decoration: none;
//     display: block;
//     text-align: left;
//   }
  
//   dropdown-content a:hover {background-color: #f1f1f1;}
  
//   dropdown:hover dropdown-content {
//     display: block;
//   }
// `