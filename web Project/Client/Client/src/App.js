
import './App.css';
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './pages/Home/Home';
import UHome from './pages/Home/UHome';
import Register from './pages/Home/Register';
import User from './pages/Home/User';
import Room from './pages/User/Room/Room';
import Index from './pages/User/Profile/Index';
import AHome from './pages/Home/AHome';
import Admin from './pages/Home/Admin'
import ViewRoom from './pages/Admin/ViewRoom/ViewRoom'
import ARoom from './pages/Admin/AddRoom/ARoom';
import Cart from './pages/Home/Cart';
import Ubooking from './pages/Home/Ubooking';
import Abooking from './pages/Home/Abooking';
import Aedit from './pages/Home/Aedit';
import Booked from './pages/Home/Booked';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
         <Routes>
           <Route path="/" element={<Home/>}/>
           <Route path="/User/signIn" element={<UHome/>}/>
           <Route path="/User/signup" element={<Register/>}/>
           <Route path="/User-Home" element={<User/>}/>
           <Route path="/Room" element={<Room/>}/>
           <Route path="/User/Profile" element={<Index/>}/>
           <Route path="/User/cart" element={<Cart/>}/>
           <Route path="/User/My-Booking" element={<Ubooking/>}/>
           <Route path="/User/book" element={<Booked/>}/>


           {/* Admin */}
           <Route path="/Admin/signIn" element={<AHome/>}/>
           <Route path="/Admin-Home" element={<Admin/>}/>
           <Route path="/ViewRoom" element={<ViewRoom/>}/>
           <Route path="/Admin/addroom" element={<ARoom/>}/>
           <Route path="/Admin/booking" element={<Abooking/>}/>
           <Route path="/Admin/editroom" element={<Aedit/>}/>
         </Routes>
      </BrowserRouter>
      <ToastContainer theme="colored" />
    </div>
  );
}


export default App;
