import Common from '../../../components/Common';
import styled from 'styled-components';
import Profile from './Profile'


const Index = () => {
    return (
        <div>
         
          <div>
             <Common/>
          </div> 
           
           <Background>
                <img src="https://cdni.iconscout.com/illustration/premium/thumb/update-your-profile-for-fitness-measurement-in-mobile-1946837-1648364.png" alt="profile-img" />
            </Background>
           <Profile/>
          
        </div>
    )
}
export default Index;

const Background = styled.div`
    position: fixed;
    top:8;
    left:10;
    right:10;
    bottom:10;
   
    opacity:0.8;

    img {
        width:75%;
        height:75%;
        margin-top: 5%;
        margin-left: 30%;
        
       
    }
`