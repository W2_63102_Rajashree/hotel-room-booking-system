import axios from "axios";
import { useState } from "react";
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router'
import { URL } from '../../../config'



function Update() {
  const [mobile, setMobile] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  

  const { userid } = sessionStorage;
  const navigate = useNavigate()

  const UpdateUser = () =>{
    if(mobile.length === 0) {
      toast.warning('Please enter mobile number')
    } else if (email.length === 0) {
      toast.warning('Please enter email')
    } else if (password.length === 0) {
      toast.warning('Please enter password')
    } else {
      const body = {
        mobile,
        email,
        password,
      }

      const url = `${URL}/Profile/edit/${userid}`

      axios.put(url, body).then((response) => {

        const result = response.data
        // console.log(result)
        if (result['status'] === 'success') {
          toast.success('User Updated!!!')
          
          navigate('/User-Home');
        }else {
          toast.error(result['error'])
        }
      })
    }
  }
  return (
    <div className="User">
      <div className="row">
        <div className="col"></div>
        <div className="col"></div>
        <div className="col">
        <h1 className="title"><u>Update Profile</u></h1>
          <div className="form">
           

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Mobile 
              </label>
              <input
                onChange={(e) => {
                  setMobile(e.target.value);
                }}
                type="text"
                className="form-control"
                
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Email
              </label>
              <input
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                type="text"
                className="form-control"
                
              />
            </div>
           
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Password
              </label>
              <input
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                type="password"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <button onClick={UpdateUser} className="btn btn-primary">Update</button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
    
  );
}

export default Update;
