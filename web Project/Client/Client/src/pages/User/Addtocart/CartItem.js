import React from "react";
import { useNavigate } from "react-router";
import { useState, useEffect } from "react";
import { URL } from "../../../config";
import axios from "axios";
import { toast } from "react-toastify";
import styled from 'styled-components';
import './CartItem.css'
import { useLocation } from "react-router";
import { CardActionArea, TableSortLabel } from "@material-ui/core";

const CartItem = () => {


  const { userid } = sessionStorage;
  
  const [cart, setcart] = useState([]);
  // const [quantity, setquantity] = useState(1);
  // const [amount, setamount] = useState(0);
  // const [total, setprice] = useState();
  const [uorders, setorder] = useState([]);
  

  const navigate = useNavigate()

  const viewcart = () => {
    const url = `${URL}/Selectfromcart/${userid}`

    axios.get(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        setcart(result["data"]);
        console.log("Result: ", result)
        let { cartid, title, description, thumbnail, quantity, price, id } = result["data"][0];
        console.log(response.data)

        // persist the logged in user's information for future use
        sessionStorage["cartid"] = cartid;
        sessionStorage["title"] = title;
        sessionStorage["description"] = description;
        sessionStorage["thumbnail"] = thumbnail;
        sessionStorage["quantity"] = quantity;
        sessionStorage["price"] = price;
        sessionStorage["roomid"] = id;
        
        
      } else {
        toast.error(result['error'])
      }

    })
  }

  // const submit = (c) => {
  //   console.log("in submit")
  //   const body = {c.quantity}


  //   const url = `${URL}/Updatecart/${c.cartid}`
  //   axios.put(url).then((response) => {
  //     const result = response.data
  //       if (result['status'] == 'success') {
  //         toast.success('menu deleted')
  //         viewcart()
  //       } else {
  //         toast.error(result['error'])
  //       }
  //   });
  // }

  const deletefromcart = (cartid) => {
    console.log(cartid)
    const url = `${URL}/Deletefromcart/${cartid}`

    axios.delete(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        toast.success('room deleted')
        viewcart()
      } else {
        toast.error(result['error'])
      }
    });
  };

  // const emptycart = (userid) => {
  //   console.log(userid)
  //   const url = `${URL}/emptycart/${userid}`

  //   axios.delete(url).then((response) => {
  //     const result = response.data
  //     if (result['status'] == 'success') {
      
  //       navigate('/User/Address');
  //     } else {
  //       toast.error(result['error'])
  //     }
  //   });
  // };

  const updatecart = (cartid, quantity) => {
    console.log(cartid)
    console.log(quantity)
    const url = `${URL}/Updatecart/${cartid}/${quantity}`

    axios.put(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        viewcart()
      } else {
        toast.error(result['error'])
      }
    });
  };

  // const vieworder= () => {
  //   const url = `${URL}/Vieworders`

  //   axios.get(url).then((response) => {
  //     const result = response.data
  //     if (result['status'] == 'success') {
  //       setorder(result["data"]);
  //       console.log("Result: ", result)
  //       let { id ,productId,price,quantity} = result["data"];
  //       console.log(response.data)
  //       sessionStorage["orderId"] = id;
  //       sessionStorage["menuId"] = productId;
  //       sessionStorage["price"] = price;
  //       sessionStorage["quantity"] = quantity;
  //       // console.log(orderId);
  //     } else {
  //       toast.error(result['error'])
  //     }
  //   });
  // };
  
  // const orderId = sessionStorage.getItem('orderId');
  const bookDetails  = (userid) => {
    // const orderId = sessionStorage.getItem('orderId');

    // console.log(id);
    console.log("in bookDetails ")
    const url=`${URL}/bookDetails/${userid}`

    axios.post(url).then((response) => {

      const result = response.data
      console.log(result)
      if (result['status'] === 'success') {
        toast.success('User successfully added!!!')
      }else {
        toast.error(result['error'])
      }
    })
  }

  const booking = (userid,total) => {
    const url = `${URL}/Addbooking/${userid}/${total}`

    axios.post(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        // vieworder();
        bookDetails(userid);
        // emptycart(userid);
        navigate('/User/Book');
      } else {
        toast.error(result['error'])
      }
    });
  };

  let sum1 = 0

  const quantitychange = async (cartid, quantity) => {
    console.log(`id: ${cartid}, quantity = ${quantity}`);
    if(quantity === 0){
      await deletefromcart(cartid);
    }else{
     await updatecart(cartid, quantity);
    }
    viewcart()
  };





  useEffect(() => {
    viewcart()
  }, [])

  // let sum = c.quantity * c.price
  let sum2 = 0
  return (

    <div className="container mx-auto mt">
     <div>
     <h1 className="title"><u>Cart</u></h1>
     </div>
     <hr/>
      {cart.map((c) => (

        <div class="row">
          <div class="col">
            <img src={'http://localhost:4000/' + c.thumbnail} class="card-img-top" width="100px" height="100px" />
          </div>
          <div class="col">
            <h6>{c.title}</h6>
          </div>
          <div class="col">
            <h6>{c.price}</h6>
          </div>
          <div class="col">
            <input type="button" value="+" class="button-minus border rounded-circle  icon-shape icon-sm mx-1 " onClick={() => quantitychange(c.cartid, c.quantity + 1)} data-field="quantity" />
            <h6>{c.quantity}</h6>
            <input type="button" value="-" class="button-plus border rounded-circle icon-shape icon-sm " onClick={() => quantitychange(c.cartid, c.quantity - 1)} data-field="quantity" />
          </div>
          <div class="col">
            <a href="#!" class="text-danger" onClick={() => deletefromcart(c.cartid)}>Remove<i class="fas fa-trash fa-lg"></i></a>
          </div>
          <div class="col">
            <noscript>{Math.round(sum2 = sum2 + c.total)}</noscript>
          </div>
        </div>
      ))}
      <hr/>
      <h4>Total Bill = {sum2}</h4>
     
      <button class="button" onClick={() => booking(userid,sum2)}>Booking</button>
      

    </div>

  )
}
export default CartItem;




