import React from "react";
import { useNavigate } from "react-router";
import { useState, useEffect } from "react";
import { URL } from "../../../config";
import axios from "axios";
import { toast } from "react-toastify";
import '../../../../node_modules/bootstrap/dist/css/bootstrap.min.css'

// import CardGroup from "@material-ui/core/CardGroup";

const Booking = () => {
  const { userid } = sessionStorage;
  const [order, setorder] = useState([]);

  const searchbooking = () => {
    
    const url = `${URL}/booking/${userid}`

    axios.get(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        setorder(result["data"]);
       } else {
        toast.error(result['error'])
      }
    });
  };


  

  useEffect(() => {
    searchbooking();
    console.log("getting called");
  }, []);

  return (
    <div className='container'>
      <h1 className="title"><u>My Booking</u></h1>
      <table className="table table-hover" id="reqTable">
        <thead>
          <tr>
            <th scope="col">Booking Id</th>
            <th scope="col">Total</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {order.map((o) => (
            <tr>
              <th scope="row">{o.id}</th>
              <td>{o.total}</td>
              <td>{o.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
export default Booking;


