import axios from "axios";
import { useState, useEffect } from "react";
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router'
import { URL } from '../../../config'



function Ubook() {

    const { userid } = sessionStorage;
    const [check_in, setCheck_in] = useState("");
    const [check_out, setCheck_out] = useState("");
    const [address, setAddress] = useState("");
   
    const navigate = useNavigate()

    const addbook = () => {
        if (check_in.length === 0) {
            toast.warning('Please enter check_in')
        } else if (check_out.length === 0) {
            toast.warning('Please enter check_in')
       } else if (address.length === 0) {
            toast.warning('Please enter address')
        } else {
            const body = {
                check_in,
                check_out,
                address,
             }

            const url = `${URL}/Book/${userid}`

            axios.post(url, body).then((response) => {
                const result = response.data
                console.log(result)
                if (result['status'] === 'success') {

                    toast.success('Room Booking Successfully')
                    navigate('/User-Home');
                } else {
                    toast.error(result['error'])
                }
            })
        }
    }

    const viewbooking= () => {
        const url = `${URL}/Viewbooking/${userid}`
    
        axios.get(url).then((response) => {
          const result = response.data
          if (result['status'] === 'success') {
         } else {
            toast.error(result['error'])
          }
    
        })
    }

    useEffect(() => 
    {
        viewbooking();
        console.log("getting called");
    }, []);

    return (
        <div className="User">

            <div className="row">
                <div className="col"></div>
                <div className="col"></div>
                <div className="col">
                    <h3 className="title"><u>Booking</u></h3>
                    <div className="form">
                        <div className="mb-3">
                            <label className="label-control">Check_in</label>
                            <input
                                onChange={(e) => {
                                    setCheck_in(e.target.value);
                                }}
                                type="text"
                                className="form-control"
                            />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="" className="label-control">
                            Check_out
                            </label>
                            <input
                                onChange={(e) => {
                                    setCheck_out(e.target.value);
                                }}
                                type="text"
                                className="form-control"
                            />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="" className="label-control">
                            Address
                            </label>
                            <input
                                onChange={(e) => {
                                    setAddress(e.target.value);
                                }}
                                type="text"
                                className="form-control"
                            />
                        </div>

                        
                        <div className="mb-3">
                            <button onClick={addbook} className="btn btn-primary">Room Booking</button>
                        </div>
                    </div>
                </div>
                <div className="col"></div>
            </div>
        </div>

    );
}

export default Ubook;
