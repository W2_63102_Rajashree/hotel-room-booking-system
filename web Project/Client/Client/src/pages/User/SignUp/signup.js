import axios from "axios";
import { useState } from "react";
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router'
import { URL } from '../../../config'
import '../SignIn/index.css'


function Register() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobile, setMobile] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const navigate = useNavigate()

  const registerUser = () =>{
    if (firstName.length === 0) {
      toast.warning('Please enter first name')
    }else if (lastName.length === 0) {
      toast.warning('Please enter last name')
    }else if (mobile.length === 0) {
      toast.warning('Please enter mobile number')
    } else if (email.length === 0) {
      toast.warning('Please enter email')
    } else if (password.length === 0) {
      toast.warning('Please enter password')
    }  else if (password !== confirmPassword) {
      toast.warning('password does not match')
    } else {
      const body = {
        firstName,
        lastName,
        mobile,
        email,
        password,
      }

      const url = `${URL}/Signup/signup`

      axios.post(url, body).then((response) => {

        const result = response.data
        console.log(result)
        if (result['status'] === 'success') {
          toast.success('User successfully added!!!')

          
          
          navigate('/User/signIn');
        }else {
          toast.error(result['error'])
        }
      })
    }
  }
  return (
    <div className="User">
     
     <div className="row">
        <div className="col"></div>
        <div className="col"></div>
        <div className="col">
        <h1 className="title"><u>Register</u></h1>
          <div className="form">
            <div className="mb-3">
              <label className="label-control">First Name</label>
              <input
                onChange={(e) => {
                  setFirstName(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Last Name
              </label>
              <input
                onChange={(e) => {
                  setLastName(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Mobile No.
              </label>
              <input
                onChange={(e) => {
                  setMobile(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Email
              </label>
              <input
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                type="text"
                className="form-control"
              />
            </div>
           
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Password
              </label>
              <input
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
                type="password"
                className="form-control"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Confirm Password
              </label>
              <input
                onChange={(e) => {
                  setConfirmPassword(e.target.value);
                }}
                type="password"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <button onClick={registerUser} className="btn btn-primary">Register</button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
    
  );
}

export default Register;
