import { useState } from 'react'
import axios from 'axios'
import { URL } from '../../../config'
import { useNavigate } from 'react-router'
import { toast } from 'react-toastify'
import { Link } from 'react-router-dom'
import './index.css'

const Asignin = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate();

  const AdminLogin = () => {
    console.log("hello")
    if (email.length == 0) {
      toast.warning("please enter email");
    }else if (password.length == 0) {
      toast.warning("please enter password");
    }else{
      const body = {
        email,
        password,
      };

      const url = `${URL}/AsignIn/asignin`;

      // make api call using axios
      axios.post(url, body).then((response) => {
        console.log("hello")
        // get the server result
        const result = response.data;
        console.log(result)
        console.log(result);
        if (result["status"] == "success") {
          toast.success("Welcome to the application");
          

          // get the data sent by server
          const {id, firstName, lastName, email, password } = result["data"];

          // persist the logged in user's information for future use
          sessionStorage["userid"] = id;
          sessionStorage["firstName"] = firstName;
          sessionStorage["lastName"] = lastName;
          sessionStorage["email"] = email;
          sessionStorage["password"] = password;
          sessionStorage["loginStatus"] = 1;

          // navigate to home component
          // navigate("/home");
           navigate("/Admin-Home");
          // navigate("/Menu");
        } else {
          toast.error("Invalid user name or password");
        }
      });
    }
  };

  
  return (
    <div className='signin'>
      <h1 className="title"><u>Login</u></h1>
      <div className="row">
      <div className="col"></div>
        <div className="col">
          <div className="form">
            <div className="mb-3">
              <label className="label-control" id='lab1' >
                Email 
              </label>
              <input
                onChange={(e) => {
                  setEmail(e.target.value)
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="label-control" id='lab1' >
                Password
              </label>
              <input
                onChange={(e) => {
                  setPassword(e.target.value)
                }}
                type="password"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <button onClick={AdminLogin}  className="btn-primary" >
                Signin
              </button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  )
}

export default Asignin
