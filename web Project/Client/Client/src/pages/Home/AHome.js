import Common from "../../components/Common";
import Asignin from "../User/SignIn/asignin";
import styled from "styled-components";


const AHome = () => {
    return (
        <div>
          
    
           <Common/>
           <Background>
                <img src="https://cashbox365.com/Front/img/help-desk.jpg" alt="admin-img" />
            </Background>
           <Asignin/>
            
           
        </div>
    )
}
export default AHome;

const Background = styled.div`
    position: fixed;
    top:20;
    left:10;
    
    right:10;
    z-index:-1;
    opacity: 0.8;
    
    img {
        width:30%;
        height:90%;
        object-fit:cover;
        margin-right: 70%;
    }
`