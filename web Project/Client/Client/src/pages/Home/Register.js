import Common from "../../components/Common";
import SignUp from "../User/SignUp/signup"
import styled from 'styled-components';


const Register = () => {
    return (
        <div>
          <div>
             <Common/>
          </div> 
           
           <Background>
                <img src="https://img.freepik.com/free-vector/booking-hotel-online-illustration-young-girl-uses-smartphone-online-reservation-cartoon-tourist-with-suitcase-paris-hotel-with-eiffel-tower-view-mobile-service-traveling_575670-2020.jpg" />
            </Background>
           <SignUp/>
          
        </div>
    )
}
export default Register;

const Background = styled.div`
    position: fixed;
    top:8;
    left:0;
    right:10;
   
    opacity:0.8;

    img {
        margin-top: 5%;
        margin-left: 30%;
        width:100%;
        height:100%;
       
    }
`