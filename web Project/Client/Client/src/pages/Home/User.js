
import UHeader from "../../components/UHeader";
import styled from 'styled-components';
import Room from "../User/Room/Room"
//import Index from "../User/Menu";


const UHome = () => {
    return (
        <div>
         
    
          
           <UHeader/>
          <Room/>
          
          
        </div>
    )
}
export default UHome;

const Container = styled.div`
  position: relative;
`;

const Background = styled.div`
    position: fixed;
    top:10;
    left:10;
    bottom:10;
    right:10;
    z-index:-1;
    opacity: 0.8;

    img {
        width:100%;
        height:50%;
        object-fit:cover;
    }
`