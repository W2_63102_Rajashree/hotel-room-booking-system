import styled from 'styled-components';
import ViewRoom from "../Admin/ViewRoom/ViewRoom"
import AHeader from "../../components/AHeader"
//import Index from "../User/Menu";


const AHome = () => {
    return (
        <div>
         
          
           <AHeader/>
           <ViewRoom/>
          
          
        </div>
    )
}
export default AHome;

const Container = styled.div`
  position: relative;
`;

const Background = styled.div`
    position: fixed;
    top:10;
    left:10;
    bottom:10;
    right:10;
    z-index:-1;
    opacity: 0.8;

    img {
        width:100%;
        height:50%;
        object-fit:cover;
    }
`