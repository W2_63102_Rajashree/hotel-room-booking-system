import Common from "../../components/Common";
import SignIn from "../User/SignIn/signin";
import styled from 'styled-components';


const UHome = () => {
    return (
        <div>
          
          
           <Common/>
           <Background>
                <img src="https://img.freepik.com/free-vector/mobile-login-concept-illustration_114360-83.jpg" alt="User-img" />
            </Background>
           <SignIn/>
            
           
        </div>
    )
}
export default UHome;

const Background = styled.div`
    position: fixed;
    top:10;
    left:5;
    bottom:10;
    right:8;
    z-index:-1;
    opacity: 0.8;

    img {
        width:75%;
        height:100%;
        object-fit:cover;
    }
`