import React from "react";
// import { useNavigate } from "react-router";
import { useState, useEffect } from "react";
import { URL } from "../../../config";
import axios from "axios";
import { toast } from "react-toastify";
import '../../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import { Form, Col, Button, Row } from 'react-bootstrap'

const Abooking = () => {

  // const { userid } = sessionStorage;
  const [abooking, setabooking] = useState([]);
  const [bookingidlist, setBookingidlist] = useState([]);
  const [bookingId, setBookingId] = useState(0);


  const viewbooking = () => {

    const url = `${URL}/allbooking/${bookingId}`;
    console.log(url);
    axios.get(url).then((response) => {
      const result = response.data;
      if (result["status"] === "success") {
        setabooking(result["data"]);
        //get the data sent by server
        console.log("Result: ", result.data)
        console.log("Resutl "+result)

        // persist the logged in user's information for future use
        
      } else {
        toast.error(result["error"]);
        console.log(result);
      }
    });
  };
  
  const viewidlist = () => {

    const url = `${URL}/allbooking/`;
    
    axios.get(url).then((response) => {
      const result = response.data;
      if (result["status"] === "success") {
        setBookingidlist(result.data);
        //get the data sent by server
        console.log("Result: ", result)
        console.log("Resutl "+result)

        // persist the logged in user's information for future use
        
      } else {
        toast.error(result["error"]);
      }
    });
  };
  
  const Accept = (bookingId) => {
    console.log("BookingId"+bookingId)
    const url = `${URL}/Accept/Accept/${bookingId}`
    axios.put(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        // toast.success('item added to cart')
          // window.location.reload()
          viewbooking()
      } else {
        toast.error(result['error'])
      }
    });
  };

  const Prepare = (bookingId) => {
    console.log("BookingId"+bookingId)
    const url = `${URL}/Accept/Prepare/${bookingId}`

    axios.put(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        // toast.success('item added to cart')
        // vieworder()
        // window.location.reload()
        viewbooking()
      } else {
        toast.error(result['error'])
      }
    });
  };

  const Out = (bookingId) => {
    console.log("BookingId"+bookingId)
    const url = `${URL}/Accept/Out/${bookingId}`

    axios.put(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        // toast.success('item added to cart')
        // vieworder()
        // window.location.reload()
        viewbooking()
      } else {
        toast.error(result['error'])
      }
    });
  };

  const Delete = (bookingId) => {
    console.log("BookingId"+bookingId)
    const url = `${URL}/Accept/Delete/${bookingId}`

    axios.delete(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        // toast.success('item added to cart')
        // vieworder()
        // window.location.reload()
        viewbooking()
      } else {
        toast.error(result['error'])
      }
    });
  };

  const Booked = (bookingId) => {
    console.log("BookingId"+bookingId)
    const url = `${URL}/Accept/Booked/${bookingId}`

    axios.put(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        // toast.success('item added to cart')
        Delete(bookingId)
        viewbooking()
      } else {
        toast.error(result['error'])
      }
    });
  };

  useEffect(() => {
    viewidlist();
    console.log("view room booking");
  }, []);

  let sum2 =0
  return (
    <div className='container'>
      <h1>Booking</h1>
      <br />
      <Row>
        <Col>
        <Row>
            <Form.Group as={Col} xs={4}>
              <Form.Label><b>BookingId</b></Form.Label>
            </Form.Group>
            <Form.Group as={Col} xs={8}>
              <Form.Select aria-label="Default select example" onChange={(e) => setBookingId(e.target.value)}>
                <option>select BookingId</option>
                { bookingidlist.map((e) => {
            return (
                
                <option>{e.id}</option>
                )
              })}
              </Form.Select>
          </Form.Group>
        </Row>
        </Col>
        <Col>
        <Form.Group as={Col} xs={5}>
          <button class="btn btn-outline-success mr-2 btn-sm" onClick={viewbooking} >See_Booking</button>
        </Form.Group>
        </Col>
      </Row>
          
        

      <br />
      <table className="table table-hover" id="reqTable">
        <thead>
          <tr>
            <th scope="col">Booking Id</th>
            <th scope="col">Title</th>
            <th scope="col">Quantity</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        <tbody>
          {abooking.map((a) => (
            <tr>
              <td>{a.bookingId}</td>
              <td>{a.title}</td>
              <td>{a.quantity}</td>
              <td>{a.price}</td>
              <td>
              { a.status == 'Room Book' ?
                <>
                  <th><button style={{padding:'0 20% 0 0'}} class="btn btn-outline-success mr-2 btn-sm" onClick={() => Accept(bookingId)} >Accept_Booking</button></th>
                </>
              :
              <>
                { a.status == 'Booking Accepted' ?
                <>
                  <th><button style={{padding:'0 20% 0 0'}} class="btn btn-outline-success mr-2 btn-sm" onClick={() => Prepare(bookingId)} >Preparing</button></th>
                </>
                :
                <>{ a.status == 'Preparing' ?
                  <>
                    <th><button style={{padding:'0 20% 0 0'}} class="btn btn-outline-success mr-2 btn-sm" onClick={() => Out(bookingId)} >Out_for_Booking</button></th>
                  </>
                  :
                  <>{ a.status == 'Out for Booking' ?
                    <>
                      <th><button style={{padding:'0 20% 0 0'}} class="btn btn-outline-success mr-2 btn-sm" onClick={() => Booked(bookingId)} >Booking</button></th>
                    </>
                    :
                    <></>
                    }
                  </>
                  }
                </>
                }
              </>
                }
              </td>
              <noscript>{Math.round(sum2 = sum2 + (a.price*a.quantity))}</noscript>
            </tr>
          ))}
        </tbody>
      </table>
      <h4>Total Bill = {sum2}</h4>
    </div>
  );
}
export default Abooking;







