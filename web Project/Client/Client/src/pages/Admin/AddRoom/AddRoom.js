import axios from "axios";
import { useState } from "react";
import { toast } from 'react-toastify';
import { URL } from '../../../config';
import { useNavigate } from 'react-router';
//import './AAddStudent.css'

function AddRoom() {
  const [title, settitle] = useState("");
  const [price, setprice] = useState("");
  const [description, setdescription] = useState("");
  const [thumbnail, setthumbnail ]= useState(undefined);
  

  const navigate = useNavigate()

  const addRoom = () =>{
    if (title.length === 0) {
      toast.warning('Please enter room title')
    }else if (price.length === 0) {
      toast.warning('Please enter room price')
    } else if (description.length === 0) {
      toast.warning('Please enter room description')
    } else if (!thumbnail) {
      toast.warning('Please select image')
    }else 
    {
      const data= new FormData()
      data.append('title',title)
      data.append('price',price)
      data.append('description',description)
      data.append('photo',thumbnail)
    

        const url = `${URL}/Addroom/`

        axios.post(url,data).then((response) => {

            const result = response.data
            console.log(result)
            if (result['status'] === 'success') {
            toast.success('Room successfully added!!!')
            
            navigate('/Admin-Home');
            }else {
            toast.error(result['error'])
            }
        })
        }
    }  
  
  return (
    <div className="AddRoom">
    <div className="row">
       <h1 className="title">Add Room</h1>
        <div className="col"></div>
        <div className="col"></div>
        
        <div className="col">
          <div className="form">
            <div className="mb-3">
              <label className="label-control">Title</label>
              <input
                onChange={(e) => {
                  settitle(e.target.value)
                }}
                type="text"
                className="form-control"
              />
            </div>


            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Description
              </label>
              <textarea
                onChange={(e) => {
                  setdescription(e.target.value)
                }}
                rows = "5"
                className="form-control"
              />
            </div>

            
            <div className="mb-3">
              <label htmlFor="" className="label-control">
                Price
              </label>
              <input
                onChange={(e) => {
                  setprice(e.target.value)
                }}
                type="text"
                className="form-control"
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="form-control">
               Thumbnail
              </label>
              <input
                onChange={(e) => {
                  setthumbnail(e.target.files[0]);
                }}
                accept="image/*"
                type="file"
                className="form-control"
              />
      
            </div>
          
            <div className="mb-3">
              <button onClick={addRoom} className="btn btn-primary">Add</button>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}

export default AddRoom;
