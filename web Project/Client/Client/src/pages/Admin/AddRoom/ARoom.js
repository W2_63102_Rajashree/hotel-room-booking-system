import Common from '../../../components/Common';
import styled from 'styled-components';
import AddRoom from './AddRoom';


const ARoom = () => {
    return (
        <div>
          <div>
             <Common/>
          </div> 
           
           <Background>
                <img src="https://img.freepik.com/premium-vector/interior-living-room-with-furniture-modern-armchair-with-mini-table-flat-cartoon-style-vector-illustration_186332-886.jpg" />
            </Background>
           <AddRoom/>
          
        </div>
    )
}
export default ARoom;

const Background = styled.div`
    position: fixed;
    top:8;
    left:10;
    right:10;
    bottom:10;
   
    opacity:0.8;

    img {
        margin-top: 20%;
        margin-right: 10%;
        width:100%;
        height:80%;
       
    }
`