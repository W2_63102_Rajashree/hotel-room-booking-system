import React from "react";
import { useNavigate } from "react-router";
import { useState, useEffect } from "react";
import { URL } from "../../../config";
import axios from "axios";
import { toast } from "react-toastify";
import '../../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './viewroom.css'

// import CardGroup from "@material-ui/core/CardGroup";

const ViewRoom = () => {

  const navigate = useNavigate();

  const [room, setroom] = useState([]);

  const searchroom = () => {
    const url = `${URL}/Room/Room`;
    axios.get(url).then((response) => {
      const result = response.data;
      if (result["status"] === "success") {
        setroom(result["data"]);
        // get the data sent by server
        const { id, title, description } = result["data"][0];
        console.log("response"+ response.data)

        // persist the logged in user's information for future use
        sessionStorage["roomid"] = id;
        sessionStorage["title"] = title;
        sessionStorage["description"] = description;
        // sessionStorage["loginStatus"] = 1;
      } else {
        toast.error(result["error"]);
      }
    });
  };

  const deleteRoom = (id) => {

    const url = `${URL}/Delete/droom/${id}`

    axios.delete(url).then((response) => {
      const result = response.data
      if (result['status'] === 'success') {
        toast.success('room deleted')
        ViewRoom()
      } else {
        toast.error(result['error'])
      }
    });
  };

  const edit = (id) => {
    navigate('/Admin/editroom',{state:{id:id}})
    console.log("in edit"+id)
  };

 
  useEffect(() => {
    searchroom();
    console.log("getting called");
  }, []);

  return (
    <div className="col">
      {room.map(m => {
        return <div style={{ width: '30%', height: '40%', display: 'inline-block', textAlign: 'center', backgroundColor: 'transparent' }}>
          <div className="card" >

            <img src={'http://localhost:4000/' + m.thumbnail} class="card-img-top" width="100px" height="250px" alt="" />

            <div className="card-title">
              <div>{m.id}</div>
              <div><b>{m.title}</b></div>
            </div>
            <div className="card-body">
              
              <div>{m.description}</div>
              <div><b>Rs.{m.price}</b></div>
              <button class="btn btn-outline-danger mr-2 btn-sm" onClick={() =>
                deleteRoom(m.id)
              }> DELETE
              </button>

              <button class="btn btn-outline-success mr-2 btn-sm" onClick={() =>
                edit(m.id)
              } type="submit" id="pro">
                EDIT
              </button>



            </div>
          </div>
        </div>
      })}
    </div>

  )

}
export default ViewRoom;


//     <div className="container" >
//     <div className="Menu">
//       <div class="title"><u><h2>Menu</h2></u></div>
//       {menu.map((m) => (
//         <div class="row">
//           <div class="col">
//              <hr class="accent my-5"></hr>
//              <div class="cards" >
//                <img src={'http://localhost:5000/'+ m.thumbnail} class="card-img-top" alt="..." height="200px" width="100px"/></div>
//                <div class="col-sm">
//                <div class="card-body">
//                <h5 class="card-title">{m.title} </h5>  
//                <p class="card-text">{m.description}</p>
//                <h5 class="card-text">Rs.{m.price}</h5>
//                <button class="btn btn-outline-success mr-2 btn-sm" 
//                  onClick={() => addtocart(m.id)} type="submit" id="pro">
//                  Add to cart
//                </button>
//                </div>
//              </div>
//            </div>
//            {/* <div class="col-sm">
//              <hr class="accent my-5"></hr>
//              <div class="card-columns">
//                <h5> {m.title} </h5>  
//                <span class="float-right font-weight-bold">{m.description}</span>
//                <h6 class="text-truncate">Rs.{m.price}</h6>
//                <button class="btn btn-outline-success mr-2 btn-sm" 
//                  onClick={() => addtocart(m.id)} type="submit" id="pro">
//                  EDIT
//                </button>
//              </div>
//            </div>
//            <div class="col-sm">
//              <hr class="accent my-5"></hr>
//              <div class="card-columns">
//                <h5> {m.title} </h5>  
//                <span class="float-right font-weight-bold">{m.description}</span>
//                <h6 class="text-truncate">Rs.{m.price}</h6>
//                <button class="btn btn-outline-success mr-2 btn-sm" 
//                  onClick={() => addtocart(m.id)} type="submit" id="pro">
//                  EDIT
//                </button>
//              </div>
//            </div>
//         </div>         */}
//         </div>
//       ))}
//   </div>
// </div>



    // <div className="container mx-auto mt">
    //   <div>
    //     <h1 className="title"><u>Menu</u></h1>
    //   </div>

    //   {menu.map((m) => (
    //     <div class="container mx-auto mt-4">
    //     <div class="row">
    //       <div class="col-md-4">
    //         <div class="card">
    //          <img src={"https:localhost:5000/+m.thumbnail"} class="card-img-top" alt="..."/>
    //       <div class="card-body">
    //         <h5 class="card-title">{m.title}</h5>
    //           <p class="card-text">{m.description}</p>
    //           <h6 class="card-subtitle mb-2 text-muted">Rs.{m.price}</h6>
    //          <a href="#" class="btn mr-2"><i class="fas fa-link"></i>Add to cart</a>
    //       </div>
    //     </div>
    //       </div>    
    //     </div>
    //     </div>
    //   )}
    //   </div>
    //   };

  // )
//   <div className="container" >
//   <div className="MenuDeatils">
//     {menu.map((m) => (
//         <div className="col-4 mt-4" id='table'>
//            <div className="accent my-5"></div>
//            <div className="card-columns" id='fontcolor'>
//            <img
//                 src={'http://localhost:5000/' + m.thumbnail}
//                alt="..."
//                className="thumbnail-sm"
//                id='image'
//              />
//               <h5 id='fontcolor'>{m.name} </h5>
//              <span className="float-right font-weight-bold" id='fontcolor'>{m.description}</span>
//              <h6 className="text-truncate" id='fontcolor'>Rs.{m.price}</h6>

//                 <button className="btn btn-outline-success mr-2 btn-sm" 
//                onClick={() => addtocart(m.menuid)} type="submit" id="pro">
//                Add to cart
//              </button>
//              <br />

//            </div>
//          </div>

//     ))}
// </div>
// </div>




