import axios from "axios";
import { useState } from "react";
import { toast } from 'react-toastify';
import { URL } from '../../../config';
import { useNavigate, useLocation } from 'react-router';


const Edit = (Props) => {
  const {state} = useLocation()
  const [title, settitle] = useState("");
  const [description, setdescription] = useState("");
  const [price, setprice] = useState("");
  // const [thumbnail, setthumbnail] = useState("");

  const navigate = useNavigate()

  const editroom = (id) => {
    if (title.length === 0) {
      toast.warning('Please enter title')
    } else if (description.length === 0) {
      toast.warning('Please enter description')
    } else if (price.length === 0) {
      toast.warning('Please enter price')
    } else {
      const body = {
        title,
        description,
        price,
       
      }

      const url = `${URL}/Edit/${state.id}`;
      console.log(url);
      axios.put(url, body).then((response) => {
        const result = response.data;
        if (result["status"] === "success") {
          toast.success('Room Updated!!!')



          navigate('/Admin-Home');
          //get the data sent by server
          console.log("Result: ", result.data)
          console.log("Resutl " + result)

          // persist the logged in user's information for future use

        } else {
          toast.error(result["error"]);
          console.log(result);
        }
      });
    }


  }

    return (
      <div className="AddRoom">

        <div className="row">

          <h1 className="title">Edit Room</h1>
          <div className="col"></div>
          <div className="col"></div>

          <div className="col">
            <div className="form">
              <div className="mb-3">
                <label className="label-control">Title</label>
                <input
                  onChange={(e) => {
                    settitle(e.target.value)
                  }}
                  type="text"
                  className="form-control"

                />
              </div>


              <div className="mb-3">
                <label htmlFor="" className="label-control">
                  Description
                </label>
                <textarea
                  onChange={(e) => {
                    setdescription(e.target.value)
                  }}
                  rows="5"
                  className="form-control"
                />
              </div>


              <div className="mb-3">
                <label htmlFor="" className="label-control">
                  Price
                </label>
                <input
                  onChange={(e) => {
                    setprice(e.target.value)
                  }}
                  type="text"
                  className="form-control"
                />
              </div>

              {/* <div className="mb-3">
                <label htmlFor="" className="form-control">
                  Thumbnail
                </label>
                <input
                  onChange={(e) => {
                    setthumbnail(e.target.files[0]);
                  }}
                  accept="image/*"
                  type="file"
                  className="form-control"
                />

              </div> */}

              <div className="mb-3">
                <button onClick={editroom} className="btn btn-primary">Edit</button>
              </div>
            </div>
          </div>
          <div className="col"></div>

        </div>

      </div>
    );
  
}
export default Edit;
